
num_instancias = 200

text = File.open('digTreino.arff').read

text = text.split("@data")[1]
instancias = []
text.each_line do |line|
  linha = line.split(",")
  linha.each {|i| i.gsub!("\r\n", "")}
  instancias << linha
end

ins = instancias.sort_by{ |e| e[-1] }[1..-1]

final = ""

escolhidos = []
(0..9).each do |i|
	escolhidos << ins.select{|e| e[-1] == i.to_s}[0..num_instancias-1]
end
newFile = File.open("digTreino#{num_instancias}.arff", "w")

escolhidos.each do |i|
	i.each do |j|
		newFile.puts "#{j.join(",")}"
	end
end


newFile.close

#i.each do |l|
#	p l[-1]
#end
